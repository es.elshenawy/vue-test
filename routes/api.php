<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\APIController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('getcategories', [APIController::class,'getcategories']);
Route::get('getSubcategory', [APIController::class,'getSubcategory']);
Route::get('getproperties', [APIController::class,'getproperties']);
Route::get('getBrands', [APIController::class,'getBrands']);
Route::get('getModel', [APIController::class,'getModel']);
Route::get('getType', [APIController::class,'getType']);
