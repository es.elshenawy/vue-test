<?php
  
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use Illuminate\Support\Facades\Http;

class APIController extends Controller
{
    /**
     * getcategories.
     *
     * @return void
     */
    public function getcategories()
    {
        $response = Http::get('https://staging.mazaady.com/api/get_all_cats');
   
        return response()->json($response->object());
    }
   
    /**
     * getSubcategory.
     *
     * @return void
     */
    public function getSubcategory(Request $request)
    {
        $response = Http::get('https://staging.mazaady.com/api/get_all_cats');
        $results = array_map(function ($prod) use ($request) {
            if ($prod['id'] === (int)$request->category_id) {
                return $prod['children'];
            }
        }, $response->json()['data']['categories']);
        return response()->json($results[0]);
    }


    /**
     * getproperties.
     *
     * @return void
     */
    public function getproperties(Request $request)
    {
        $response = Http::get('https://staging.mazaady.com/api/properties?cat='.$request->child_id);
       
        if ($response->json()['data'] != null) {
            foreach ($response->json()['data'] as $prod) {
                if ($prod['id'] === (int)$request->parent_id) {
                    return response()->json($prod['options']);
                }
            }
        }
    }

    /**
    * getBrands.
    *
    * @return void
    */
    public function getBrands(Request $request)
    {
        if ($request->property_id) {
            $response = Http::get('https://staging.mazaady.com/api/properties?cat='.$request->child_id);
            $brands = [];
            foreach ($response->json()['data'] as $prod) {
                if ($prod['slug'] === "brand") {
                    return response()->json($prod['options']);
                }
            }
        }
    }


    /**
     * getModel.
     *
     * @return void
     */
    public function getModel(Request $request)
    {
        if($request->brand_id){
            $response = Http::get('https://staging.mazaady.com/get-options-child/'.$request->brand_id);
            return response()->json($response->json()['data'][0]['options']);
        }
    }

    /**
     * getType.
     *
     * @return void
     */
    public function getType(Request $request)
    {
        if ($request->model_id) {
            $response = Http::get('https://staging.mazaady.com/get-options-child/'.$request->model_id);
            foreach ($response->json()['data'] as $prod) {
                return response()->json($prod['options']);
            }
        }
    }
}
